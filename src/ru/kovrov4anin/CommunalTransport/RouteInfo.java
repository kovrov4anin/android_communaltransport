package ru.kovrov4anin.CommunalTransport;

import android.app.Activity;
import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.Shape;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.google.gson.Gson;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.map.MapLayer;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import android.app.ActionBar;
import java.util.ArrayList;
import java.util.List;



public class RouteInfo extends Activity implements ReceivWebData
{
    private LinearLayout LinearLayout1;
    private LinearLayout LinearLayout2;
    private LinearLayout LinearLayout3;
    private TabHost tabHost;
    private TownDataFull.SectionDataFull.RouteDataFull rdf;
    private Web webConnection;
    private MapController mMapController;
    private MyOverlay myOverlay;
    private Overlay RoutePointsOverlay;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= 14) {
            try{
                ActionBar actionBar = this.getActionBar();
                actionBar.setIcon(getIntent().getIntExtra("Icon", R.drawable.ymk_empty_image));
                actionBar.setDisplayHomeAsUpEnabled(true);
            } catch (Exception e) {}
        }

        this.setTitle(getIntent().getStringExtra("Caption"));
        setContentView(R.layout.route_info);


        LinearLayout1 = (LinearLayout) findViewById(R.id.LinearLayout1);
        LinearLayout2 = (LinearLayout) findViewById(R.id.LinearLayout2);
        LinearLayout3 = (LinearLayout) findViewById(R.id.LinearLayout3);
        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        String json = getIntent().getStringExtra("RouteDataFull");
        if (!json.isEmpty())
        {
            rdf = new Gson().fromJson(json, TownDataFull.SectionDataFull.RouteDataFull.class);
            tabHost.setup();
            TabHost.TabSpec tabSpec;
            if ((rdf.Route.timetable != null) && (rdf.Route.timetable.length > 0))
            {
                tabSpec = tabHost.newTabSpec("TagTimeTable");
                tabSpec.setIndicator(getString(R.string.timetable_text));
                tabSpec.setContent(R.id.ScrollView1);
                tabHost.addTab(tabSpec);
                for (int i = 0; i < rdf.Route.timetable.length; i++) addDetailString(LinearLayout1, rdf.Route.timetable[i]);
            }
            else
            {
                LinearLayout1.removeAllViews();
                LinearLayout1.setEnabled(false);
            }
            if (rdf.RoutePointsData.length > 0)
            {
                tabSpec = tabHost.newTabSpec("TagRoutPoints");
                tabSpec.setIndicator(getString(R.string.rout_mov_text));
                tabSpec.setContent(R.id.ScrollView2);
                tabHost.addTab(tabSpec);
                for (int i = 0; i < rdf.RoutePointsData.length; i++)  addDetailString(LinearLayout2, rdf.RoutePointsData[i].Name, i);
                if (isRouteLocated(rdf.RoutePointsData) && (isOnline()))
                {
                    tabSpec = tabHost.newTabSpec("TagMap");
                    tabSpec.setIndicator(getString(R.string.map_text));
                    tabSpec.setContent(R.id.LinearLayout3);
                    tabHost.addTab(tabSpec);

                    String param1 = "route_id=" + rdf.Route.id;
                    webConnection = new Web(this);
                    webConnection.RequestData(getString(R.string.url_name), Web.RequestType.FinePointList, null, param1);
                    try {
                        final MapView mapView = new MapView(getApplicationContext(), "wIqJ6K-GXQdfEbpaWpkiL80vIQCtsCCnSK1emAQzT-TN~hOLHV0OFaaloaR1E7CnSYIstMCSbERIPlI7MuftfaMaQM2NMWp-I7inbta0Bm0=");
                        mMapController = mapView.getMapController();
                        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        LinearLayout3.addView(mapView, llp);
                        mapView.showFindMeButton(true);
                        mapView.showZoomButtons(true);
                        mapView.showJamsButton(false);
                        mMapController.setHDMode(true);
                        mMapController.getOverlayManager().getMyLocation().setEnabled(true);
                        if (rdf.UsePeopleMap == 1)
                        {
                            MapLayer ml = mMapController.getMapLayerByLayerRequestName(getString(R.string.people_map_constant));
                            mMapController.setCurrentMapLayer(ml);
                        }
                        ArrayList<GeoPoint> geoPoints = RoutePointsToGeoPointList(rdf.RoutePointsData);
                        MappingRoute(rdf.Route.route_type, geoPoints);
                        MappingPoints(rdf.RoutePointsData);
                        setZoomSpan(geoPoints);
                    }
                    catch (Exception e){}
                }
                else
                {
                    LinearLayout3.setEnabled(false);
                }
            }
            else
            {
                LinearLayout2.setEnabled(false);
                LinearLayout3.setEnabled(false);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home) finish();
        return false;
    }


    @Override
    public void receiveList(Web.RequestType Type, ArrayList<?> list, Object Calling)
    {
        if (Type == Web.RequestType.FinePointList)
        {
            if ((list != null) && (list.size() > rdf.RoutePointsData.length)) {
                ArrayList<GeoPoint> geoPoints = (ArrayList<GeoPoint>) list;
                MappingRoute(rdf.Route.route_type, geoPoints);
            }
        }
    }


    private ArrayList<GeoPoint> RoutePointsToGeoPointList(Web.RoutePoints[] rp)
    {
        ArrayList<GeoPoint> geoPoints = new ArrayList<GeoPoint>();
        for (int i = 0; i < rp.length; i++) {
            if (rp[i] != null) {
                GeoPoint gp = new GeoPoint(rp[i].Latitude, rp[i].Longitude);
                geoPoints.add(gp);
            }
        }
        return geoPoints;
    }


    private void MappingRoute(RouteLine.TransportType Type, List<GeoPoint> points)
    {
        if (myOverlay != null) mMapController.getOverlayManager().removeOverlay(myOverlay);
        int color = getColor(Type);
        myOverlay = new MyOverlay(mMapController, points, color);
        myOverlay.setPriority((byte)0);
        mMapController.getOverlayManager().addOverlay(myOverlay);
        setZoomSpan(points);
    }


    private int getColor(RouteLine.TransportType Type)
    {
        switch (Type) {
            case BUS:
                return Color.rgb(123, 203, 52);
            case TROLLEYBUS:
                return Color.rgb(49, 139, 206);
            case TRAM:
                return Color.rgb(239, 128, 16);
            case TRAIN:
                return Color.rgb(235, 20, 20);
            case METRO:
                return Color.rgb(179, 71, 184);
            default:
                return Color.GRAY;
        }
    }


    private void MappingPoints(Web.RoutePoints[] routePoints)
    {
        if (RoutePointsOverlay != null) mMapController.getOverlayManager().removeOverlay(RoutePointsOverlay);
        Shape shapeCircle = new Shape() {
            @Override
            public void draw(Canvas canvas, Paint paint) {;
                paint.setColor(getColor(rdf.Route.route_type));
                paint.setStyle(Paint.Style.FILL);
                canvas.drawCircle(18, 18, 10, paint);
                paint.setColor(Color.BLACK);
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeWidth(2);
                canvas.drawCircle(18, 18, 10, paint);
            }
        };
        ShapeDrawable shapeDrawableCircle = new ShapeDrawable(shapeCircle);
        shapeDrawableCircle.setIntrinsicHeight(36);
        shapeDrawableCircle.setIntrinsicWidth(36);
        RoutePointsOverlay = new Overlay(mMapController);
        for (Web.RoutePoints rp : routePoints) {
            OverlayItem PointOverlay = new OverlayItem(new GeoPoint(rp.Latitude, rp.Longitude), shapeDrawableCircle);
            BalloonItem PointBalloon = new BalloonItem(this, PointOverlay.getGeoPoint());
            PointBalloon.setText(rp.Name);
            PointOverlay.setBalloonItem(PointBalloon);
            RoutePointsOverlay.addOverlayItem(PointOverlay);
        }
        mMapController.getOverlayManager().addOverlay(RoutePointsOverlay);
    }


    private boolean isRouteLocated(Web.RoutePoints[] RoutePoints)
    {
        if ((RoutePoints == null) || (RoutePoints.length == 0)) return false;
        for (int i = 0; i < RoutePoints.length; i++) {
            if (RoutePoints[i].Latitude == 0) return false;
            if (RoutePoints[i].Longitude == 0) return false;
        }
        return true;
    }


    private boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnectedOrConnecting());
    }



    private TextView addDetailString(LinearLayout Layout, String Data)
    {
        TextView nTextView = new TextView(this);
        nTextView.setTextAppearance(this, android.R.style.TextAppearance_Medium);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.gravity = Gravity.LEFT;
        llp.setMargins(15, 2, 15, 2);
        nTextView.setText(Data);
        Layout.addView(nTextView, llp);
        return nTextView;
    }

    private void addDetailString(LinearLayout Layout, String Data, int RoutPointID)
    {
        TextView nTextView = addDetailString(Layout, Data);
        nTextView.setId(RoutPointID);
        nTextView.setClickable(true);
        nTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (mMapController != null) {
                        int index = v.getId();
                        tabHost.setCurrentTabByTag("TagMap");
                        double lat = rdf.RoutePointsData[index].Latitude;
                        double lon = rdf.RoutePointsData[index].Longitude;
                        mMapController.setPositionAnimationTo(new GeoPoint(lat, lon), 15);
                        OverlayItem oi = (OverlayItem) RoutePointsOverlay.getOverlayItems().get(index);
                        mMapController.showBalloon(oi.getBalloonItem());
                    }
                } catch (Exception e) {}
            }
        });
    }


    private void setZoomSpan(List<GeoPoint> list)
    {
        double maxLat, minLat, maxLon, minLon;
        maxLat = maxLon = Double.MIN_VALUE;
        minLat = minLon = Double.MAX_VALUE;
        for (int i = 0; i < list.size(); i++)
        {
            double lat = list.get(i).getLat();
            double lon = list.get(i).getLon();
            maxLat = Math.max(lat, maxLat);
            minLat = Math.min(lat, minLat);
            maxLon = Math.max(lon, maxLon);
            minLon = Math.min(lon, minLon);
        }
        mMapController.setZoomToSpan(maxLat - minLat, maxLon - minLon);
        mMapController.setPositionNoAnimationTo(new GeoPoint((maxLat + minLat) / 2, (maxLon + minLon) / 2));
    }

}

package ru.kovrov4anin.CommunalTransport;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import ru.yandex.yandexmapkit.overlay.IRender;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.utils.ScreenPoint;


public class MyRender implements IRender {
    private int color;
    @Override
    public void draw(Canvas canvas, OverlayItem item_) {
        Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(5);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setColor(color);
        MyOverlayItem item  = (MyOverlayItem)item_;
        Path p = new Path();
        if (item.screenPoint != null && item.screenPoint.size() > 0)
        {
            ScreenPoint screenPoint = item.screenPoint.get(0);
            p.moveTo(screenPoint.getX(), screenPoint.getY());

            for (int i = 1; i<item.screenPoint.size(); i++ ){
                screenPoint = item.screenPoint.get(i);
                p.lineTo(screenPoint.getX(), screenPoint.getY());
            }
            canvas.drawPath(p, mPaint);
        }
    }

    public MyRender(int color) {
        this.color = color;
    }
}

package ru.kovrov4anin.CommunalTransport;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.widget.*;
import com.google.gson.Gson;

/**
 * Created by User on 18.05.2015.
 */
public class RouteLine extends LinearLayout
{
    public LinearLayout.LayoutParams LayoutParams;
    private Context context;
    private TownDataFull.SectionDataFull.RouteDataFull rdf;
    private ImageView nImageView;
    private TextView nTextView1;
    private TextView nTextView2;
    private Intent RouteInfoIntent;


    public RouteLine(Context context, TownDataFull.SectionDataFull.RouteDataFull rdf)
    {
        super(context);
        this.context = context;
        this.rdf = rdf;
        RouteInfoIntent = new Intent(context, RouteInfo.class);

        LayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LayoutParams.setMargins(0, 5, 0, 5);
        LayoutParams.weight = 1;
        this.setOrientation(LinearLayout.HORIZONTAL);
        this.setClickable(true);

        nImageView = new ImageView(context);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.gravity = Gravity.CENTER_VERTICAL;
        llp.setMargins(5, 5, 5, 5);
        llp.height = 37;
        llp.width = 37;
        nImageView.setImageResource(getTransportIcon(rdf.Route.route_type));
        this.addView(nImageView, llp);

        nTextView1 = new TextView(context);
        nTextView1.setTextAppearance(context, android.R.style.TextAppearance_Large);
        llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.gravity = Gravity.CENTER_VERTICAL;
        llp.setMargins(5, 5, 5, 5);
        SetRouteNumber(Caption(rdf.Route.route_type, rdf.Route.name));
        this.addView(nTextView1, llp);

        nTextView2 = new TextView(context);
        nTextView2.setTextAppearance(context, android.R.style.TextAppearance_Small);
        llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.gravity = Gravity.CENTER_VERTICAL;
        llp.setMargins(5, 5, 5, 5);
        SetRouteDescription(rdf.Route.description);
        this.addView(nTextView2, llp);

        this.setOnClickListener(this.onRowClick);
    }


    private OnClickListener onRowClick = new OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            ShowRouteInfo();
        }
    };


    public void SetRouteNumber(String RouteNumber)
    {
        nTextView1.setText(RouteNumber);
    }


    public void SetRouteDescription(String RouteDescription)
    {
        nTextView2.setText(RouteDescription);
    }


    private int getTransportIcon(TransportType Type)
    {
        switch (Type)
        {
            case BUS:
                return R.drawable.small_avtobus;
            case TROLLEYBUS:
                return R.drawable.small_troll;
            case TRAM:
                return R.drawable.small_tram;
            case TRAIN:
                return R.drawable.small_rail;
            case METRO:
                return R.drawable.small_metro;
            default: return R.drawable.ymk_empty_image;
        }
    }


    private String Caption(TransportType Type, String RouteName)
    {
        switch (Type)
        {
            case TROLLEYBUS:
                return (context.getString(R.string.trolleybus_n_text) + " " + RouteName);
            case BUS:
                return (context.getString(R.string.bus_n_text) + " " + RouteName);
            case TRAM:
                return (context.getString(R.string.tram_n_text) + " " + RouteName);
            case TRAIN:
                return (context.getString(R.string.train_n_text) + " " + RouteName);
            case METRO:
                return (context.getString(R.string.metro_n_text) + " " + RouteName);
            default: return RouteName;
        }
    }

    private void ShowRouteInfo()
    {
        if ((rdf.RoutePointsData.length > 0) || (((rdf.Route.timetable != null) && (rdf.Route.timetable.length > 0))))
        {
            Gson gson = new Gson();
            String json = gson.toJson(rdf);
            RouteInfoIntent.putExtra("RouteDataFull", json);
            RouteInfoIntent.putExtra("Caption", Caption(rdf.Route.route_type, rdf.Route.name));
            RouteInfoIntent.putExtra("Icon", getTransportIcon(rdf.Route.route_type));
            context.startActivity(RouteInfoIntent);
        }
    }



    public enum TransportType
    {
        BUS,
        TROLLEYBUS,
        TRAM,
        TRAIN,
        METRO
    }
}

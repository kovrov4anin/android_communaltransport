package ru.kovrov4anin.CommunalTransport;

import android.app.*;
import android.content.*;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.*;
import java.net.URLEncoder;
import java.util.ArrayList;


public class TownsChoice extends Activity implements ReceivWebData, LineClicked
{
    private EditText mEditText1;
    private TableRow mTableRow2;
    private TableRow mTableRow3;
    private TownList mTownList;
    private TownList mRegionList;
    private ProgressDialog pd;
    private Web webConection;
    private Web webConectionNear;
    private Web webConectionPrename;
    private ArrayList<Web.Regions> RegionsList;
    private ArrayList<Web.Towns> TownList;
    private Geolocation geolocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.towns_choice);
        mEditText1 = (EditText)findViewById(R.id.editText1);
        mEditText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                String Prename = s.toString().trim();
                if (Prename.length() > 1) {
                    if (webConectionPrename == null) webConectionPrename = new Web(TownsChoice.this);
                    try {
                        String param1 = "q=" + URLEncoder.encode(Prename, "UTF-8");
                        String param2 = "p=1";
                        webConectionPrename.RequestData(getString(R.string.url_name), Web.RequestType.TownListPrename, null, param1, param2);
                    } catch (Exception e){}
                }
            }
        });
        mTableRow2 =(TableRow)findViewById(R.id.TableRow2);
        mTableRow3 =(TableRow)findViewById(R.id.TableRow3);
        if (webConection == null) webConection = new Web(this);
        String param2 = "p=1";
        webConection.RequestData(getString(R.string.url_name), Web.RequestType.RegionList, null, param2);
    }


    @Override
    protected void onPause()
    {
        super.onPause();
        geolocation.RemoveUpdates();
    }


    @Override
    protected void onResume()
    {
        super.onResume();
        geolocation = new Geolocation(this, 1000) {
            @Override
            void ReceiveLocation(double Latitude, double Longitude) {
                NearTownsRequest(Latitude, Longitude);
            }
        };
    }



    private void NearTownsRequest(double Latitude, double Longitude)
    {
            String param1 = "latitude=" + Latitude;
            String param2 = "longitude=" + Longitude;
            String param3 = "p=1";
            if (webConectionNear == null) webConectionNear = new Web(this);
            webConectionNear.RequestData(getString(R.string.url_name), Web.RequestType.TownListNear, null, param1, param2, param3);
    }



    public void receiveList(Web.RequestType Type, ArrayList<?> list, Object Calling)
    {
        if (list == null) return;
        if (Type == Web.RequestType.RegionList)
        {
            RegionsList = (ArrayList<Web.Regions>)list;
            mTableRow3.removeAllViews();
            mRegionList = new TownList(this, ListType.RegionsList);
            mRegionList.SetTitle(getString(R.string.regions_text));
            String names[] = new String[RegionsList.size()];
            for (int i = 0; i < RegionsList.size(); i++) names[i] = RegionsList.get(i).Name;
            mRegionList.setTownList(names, this);
            mTableRow3.addView(mRegionList, mRegionList.LayoutParams);
        }
        if (Type == Web.RequestType.TownListNear)
        {
            PrintTownsList(ListType.TounsNearList, list, getString(R.string.towns_near_text));
        }
        if (Type == Web.RequestType.TownListPrename)
        {
            mTableRow3.removeAllViews();
            PrintTownsList(ListType.TownsByPrename, list, getString(R.string.towns_text));
        }
        if (Type == Web.RequestType.TownList)
        {
            if (pd != null) pd.hide();
            mTableRow3.removeAllViews();
            PrintTownsList(ListType.TounsByRegion, list, getString(R.string.towns_text));
        }
    }


    private void PrintTownsList(LineClicked.ListType Type, ArrayList<?> list, String TitleConstant)
    {
        TownList = (ArrayList<Web.Towns>)list;
        mTableRow2.removeAllViews();
        if (TownList.size() > 0)
        {
            mTownList = new TownList(this, Type);
            String RegionId = TownList.get(0).region_id;
            boolean Equals = true;
            if (RegionId.isEmpty()) Equals = false;
            else {
                for (int i = 1; i < TownList.size(); i++) {
                    if (!TownList.get(i).region_id.equals(RegionId)) {
                        Equals = false;
                        break;
                    }
                }
            }
            if (Equals) mTownList.SetTitle(TitleConstant + " (" + FindRegionName(RegionId) + ")");
            else mTownList.SetTitle(TitleConstant);
            String names[] = new String[list.size()];
            for (int i = 0; i < TownList.size(); i++)
            {
                Web.Towns t = TownList.get(i);
                names[i] = t.Name;
                if ((!Equals) && (!t.region_id.isEmpty()))
                {
                    names[i] = names[i] + ", " + FindRegionName(t.region_id);
                }
            }
            mTownList.setTownList(names, this);
            mTableRow2.addView(mTownList, mTownList.LayoutParams);
        }
    }


    private String FindRegionName(String RegionId)
    {
        String RegionName = "";
        if (RegionsList != null)
        {
            for (Web.Regions r : RegionsList)
            {
                if (r.id.equals(RegionId))
                {
                    RegionName = r.Name;
                    break;
                }
            }
        }
        return RegionName;
    }


    @Override
    public void ListLineClicked(LineClicked.ListType Type, String Text, long Id)
    {
        if ((Type == ListType.TounsByRegion) || (Type == ListType.TownsByPrename) || (Type == ListType.TounsNearList))
        {
            if (TownList.size() > Id)
            {
                String TownId = TownList.get((int) Id).id;
                String TownName = TownList.get((int) Id).Name;
                Intent intent = new Intent();
                intent.putExtra("TownName", TownName);
                intent.putExtra("TownId", TownId);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
        if (Type == ListType.RegionsList)
        {
            if (webConection == null) webConection = new Web(this);
            String param1 = "region_id=" + RegionsList.get((int)Id).id;
            String param2 = "p=1";
            webConection.RequestData(getString(R.string.url_name), Web.RequestType.TownList, null, param1, param2);
            pd = new ProgressDialog(this);
            pd.setTitle(getString(R.string.progress_title_text));
            pd.setMessage(getString(R.string.progress_message_text));
            pd.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.cancel_text), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            pd.show();
        }

    }

}

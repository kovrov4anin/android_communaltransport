package ru.kovrov4anin.CommunalTransport;

import java.util.ArrayList;
import java.util.List;
import android.R;
import android.content.Context;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;


public class MyOverlay extends Overlay
{
    MyOverlayItem overlayRectItem;
    Context mContext;
    MapController mMapController;
    MyRender rectRender;

    public MyOverlay(MapController mapController, List<GeoPoint> list, int color) {
        super(mapController);
        mMapController = mapController;
        mContext = mapController.getContext();
        rectRender = new MyRender(color);
        setIRender(rectRender);
        if (list != null)
        {
            overlayRectItem = new MyOverlayItem(list);
            addOverlayItem(overlayRectItem);
        }
    }

    @Override
    public List<OverlayItem> prepareDraw() {
        ArrayList<OverlayItem> draw = new ArrayList<OverlayItem>();
        overlayRectItem.screenPoint.clear();
        for( GeoPoint point : overlayRectItem.geoPoint){
            overlayRectItem.screenPoint.add(mMapController.getScreenPoint(point));
        }
        draw.add(overlayRectItem);

        return draw;
    }


    @Override
    public int compareTo(Object another) {
        return 0;
    }
}

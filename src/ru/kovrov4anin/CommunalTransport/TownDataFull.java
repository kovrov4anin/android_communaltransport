package ru.kovrov4anin.CommunalTransport;

import java.util.ArrayList;

/**
 * Created by User on 24.05.2015.
 */
public class TownDataFull
{
    public SectionDataFull[] SectionsData;
    public String TownName;
    public String TownId;

    public boolean isFilled()
    {
        if (TownName.isEmpty() || TownId.isEmpty() || (SectionsData == null)) return false;
        else
        {
            for (int i = 0; i < SectionsData.length; i++)
            {
                if (SectionsData[i] == null) return false;
                else
                {
                    if (!SectionsData[i].isFilled()) return false;
                }
            }
        }
        return true;
    }

    public void initSectionsData(int Size) {
        SectionsData = new SectionDataFull[Size];
        for (int i = 0; i < Size; i++) {
            SectionsData[i] = new SectionDataFull();
        }
    }

    public class SectionDataFull
    {
        public RouteDataFull[] RoutesData;
        public Web.Sections Section;
        public ArrayList<RouteDataFull> RoutesNear;

        public boolean isFilled()
        {
            if ((RoutesData == null) || (Section == null)) return false;
            else
            {
                for (int j = 0; j < RoutesData.length; j++)
                {
                    if (RoutesData[j] == null) return false;
                    else
                    {
                        if (!RoutesData[j].isFilled()) return false;
                    }
                }
            }
            return true;
        }

        public void initRoutesData(int Size)
        {
            RoutesData = new RouteDataFull[Size];
            for (int i = 0; i < Size; i++) {
                RoutesData[i] = new RouteDataFull();
            }
        }

        public void AddFindRoute(Web.Routes Route)
        {
            if (RoutesNear == null) RoutesNear = new ArrayList<RouteDataFull>();
            if (RoutesData != null)
            {
                for (int i = 0; i < RoutesData.length; i++)
                {
                    if (Route.equals(RoutesData[i].Route)) RoutesNear.add(RoutesData[i]);
                }
            }
        }


        public class RouteDataFull
        {
            public Web.RoutePoints[] RoutePointsData;
            public Web.Routes Route;
            public int UsePeopleMap;

            public boolean isFilled()
            {
                if ((RoutePointsData == null) || (Route == null)) return false;
                else
                {
                    for (int k = 0; k < RoutePointsData.length; k++)
                    {
                        if (RoutePointsData[k] == null) return false;
                    }
                }
                return true;
            }

            public void initRoutePointsData(int Size)
            {
                RoutePointsData = new Web.RoutePoints[Size];
            }
        }
    }
}

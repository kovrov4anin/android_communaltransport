package ru.kovrov4anin.CommunalTransport;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

/**
 * Created by User on 05.06.2015.
 */
public abstract class Geolocation
{
    private LocationManager locationManager;
    private LocationListener GpsLocationListener;
    private LocationListener NetworkLocationListener;
    private Location LastLocation;
    private boolean GpsWork;

    public Geolocation(Context context, float ChangeRadius)
    {
        GpsWork = false;
        locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            GpsLocationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null)
                    {
                        GpsWork = true;
                        LastLocation = location;
                        ReceiveLocation(location.getLatitude(), location.getLongitude());
                        if (NetworkLocationListener != null) {
                            locationManager.removeUpdates(NetworkLocationListener);
                            NetworkLocationListener = null;
                        }
                    }

                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {}

                @Override
                public void onProviderEnabled(String provider) {
                    Location nLoc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if ((nLoc != null) && (LastLocation == null))
                    {
                        LastLocation = nLoc;
                        ReceiveLocation(nLoc.getLatitude(), nLoc.getLongitude());
                    }
                }

                @Override
                public void onProviderDisabled(String provider) {}
            };
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, ChangeRadius, GpsLocationListener);
        }

        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
        {
            NetworkLocationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null)
                    {
                        LastLocation = location;
                        ReceiveLocation(location.getLatitude(), location.getLongitude());
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {}

                @Override
                public void onProviderEnabled(String provider) {
                    Location nLoc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if ((nLoc != null) && (LastLocation == null))
                    {
                        LastLocation = nLoc;
                        ReceiveLocation(nLoc.getLatitude(), nLoc.getLongitude());
                    }
                }

                @Override
                public void onProviderDisabled(String provider) {}
            };
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, ChangeRadius, NetworkLocationListener);
        }
    }

    public void RemoveUpdates()
    {
        if (GpsLocationListener != null) locationManager.removeUpdates(GpsLocationListener);
        if (NetworkLocationListener != null) locationManager.removeUpdates(NetworkLocationListener);
        GpsLocationListener = null;
        NetworkLocationListener = null;
    }

    public Location getLastLocation() {
        return LastLocation;
    }

    public boolean isGpsWork() {
        return GpsWork;
    }

    abstract void ReceiveLocation(double Latitude, double Longitude);


}

package ru.kovrov4anin.CommunalTransport;


interface LineClicked
{
    void ListLineClicked(ListType Type, String Text, long Id);

    enum ListType
    {
        RegionsList,
        TounsNearList,
        TounsByRegion,
        TownsByPrename
    }
}

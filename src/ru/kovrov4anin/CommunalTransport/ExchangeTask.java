package ru.kovrov4anin.CommunalTransport;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ExchangeTask extends AsyncTask
{
    private Web webInstance;
    private String patch;
    private Web.RequestType rType;
    private Object Calling;
    private HttpURLConnection c;

    public ExchangeTask(Web webInstance, String patch, Web.RequestType rType, Object Calling)
    {
        this.webInstance = webInstance;
        this.patch = patch;
        this.rType = rType;
        this.Calling = Calling;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        String content;
        try {
            content = getContent(patch);
        } catch (IOException ex) {
            content = ex.getMessage();
        }
        return content;
    }

    @Override
    protected void onPostExecute(Object o)
    {
        if (webInstance != null) webInstance.Response(rType, (String) o, Calling);
        webInstance = null;
        rType = null;
        Calling = null;
    }

    private String getContent(String path) throws IOException {
        BufferedReader reader = null;
        StringBuilder buf = new StringBuilder();
        try {
            URL url = new URL(path);
            c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setReadTimeout(7000);
            c.connect();
            reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null)
            {
                buf.append(line + "\n");
            }
        }
        finally
        {
            if (c != null) c.disconnect();
            if (reader != null) reader.close();
        }
        return (buf.toString());
    }

    public void setWebInstance(Web webInstance) {
        this.webInstance = webInstance;
    }
}
package ru.kovrov4anin.CommunalTransport;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import ru.yandex.yandexmapkit.utils.GeoPoint;


public class Web
{
    private ExchangeTask task;
    private ReceivWebData ReceivWebDataInstance;
    private ArrayList<Object> DataList;
    private JsonArray ja;

    //public TextView textView4;

    public Web(ReceivWebData ReceivWebDataInstance)
    {
        this.ReceivWebDataInstance = ReceivWebDataInstance;
    }

    public void Response(RequestType rType, String content, Object Calling)
    {
        //if (textView4 != null) textView4.setText(textView4.getText() + " \n" + content);          //
        DataList = new ArrayList<Object>();
        int ItemCount;
        try
        {
            JsonParser jp = new JsonParser();
            ja = jp.parse(content).getAsJsonObject().getAsJsonArray("items");
            ItemCount = ja.size();
        }
        catch (Exception e)
        {
            ReceivWebDataInstance.receiveList(rType, null, Calling);
            return;
        }
            for (int i = 0; i < ItemCount; i++)
            {
                try {
                    JsonObject ijo = ja.get(i).getAsJsonObject();
                    switch (TypeQualification(rType)) {
                        case Region:
                            Regions re = new Regions();
                            re.id = ijo.getAsJsonPrimitive("id").getAsString();
                            re.Name = ijo.getAsJsonPrimitive("name").getAsString();
                            DataList.add(re);
                            break;
                        case Town:
                            Towns t = new Towns();
                            t.id = ijo.getAsJsonPrimitive("id").getAsString();
                            t.Name = ijo.getAsJsonPrimitive("name").getAsString();
                            try { t.region_id = ijo.getAsJsonPrimitive("region_id").getAsString();}
                            catch (Exception e){t.region_id = "";}
                            /*try {
                                t.Latitude = ijo.getAsJsonPrimitive("latitude").getAsDouble();
                                t.Longitude = ijo.getAsJsonPrimitive("longitude").getAsDouble();
                            }catch (Exception e){}*/
                            DataList.add(t);
                            break;
                        case Section:
                            Sections s = new Sections();
                            s.id = ijo.getAsJsonPrimitive("id").getAsInt();
                            s.town_id = ijo.getAsJsonPrimitive("town_id").getAsString();
                            s.Name = ijo.getAsJsonPrimitive("caption").getAsString();
                            try { s.use_people_maps = ijo.getAsJsonPrimitive("use_people_maps").getAsInt();}
                            catch (Exception e) {s.use_people_maps = 0;}
                            DataList.add(s);
                            break;
                        case Route:
                            Routes ro = new Routes();
                            ro.id = ijo.getAsJsonPrimitive("id").getAsInt();
                            ro.section_id = ijo.getAsJsonPrimitive("section_id").getAsInt();
                            ro.name = ijo.getAsJsonPrimitive("name").getAsString();
                            try {
                                ro.description = ijo.getAsJsonPrimitive("description").getAsString();
                            } catch (Exception e){}
                            String route_type = ijo.getAsJsonPrimitive("route_type").getAsString();
                            if (route_type.equals("trolleybus")) ro.route_type = RouteLine.TransportType.TROLLEYBUS;
                            if (route_type.equals("bus")) ro.route_type = RouteLine.TransportType.BUS;
                            if (route_type.equals("tram")) ro.route_type = RouteLine.TransportType.TRAM;
                            if (route_type.equals("train")) ro.route_type = RouteLine.TransportType.TRAIN;
                            if (route_type.equals("metro")) ro.route_type = RouteLine.TransportType.METRO;
                            try {ro.timetable = ijo.getAsJsonPrimitive("timetable").getAsString().split("\\r\\n");}
                            catch (Exception e) {ro.timetable = null;}
                            DataList.add(ro);
                            break;
                        case RoutePoint:
                            RoutePoints rp = new RoutePoints();
                            rp.id = ijo.getAsJsonPrimitive("id").getAsInt();
                            rp.Name = ijo.getAsJsonPrimitive("name").getAsString();
                            try {
                                rp.Latitude = ijo.getAsJsonPrimitive("latitude").getAsDouble();
                                rp.Longitude = ijo.getAsJsonPrimitive("longitude").getAsDouble();
                            } catch (Exception e) {}
                            DataList.add(rp);
                            break;
                        case RoutePointUi:
                            GeoPoint gp = new GeoPoint();
                            gp.setLat(ijo.getAsJsonPrimitive("latitude").getAsDouble());
                            gp.setLon(ijo.getAsJsonPrimitive("longitude").getAsDouble());
                            DataList.add(gp);
                            break;
                    }
                }
                catch (Exception e){}
            }
        SortListByText(DataList);
        ReceivWebDataInstance.receiveList(rType, DataList, Calling);
    }

    public void RequestData(String Host, RequestType rType, Object Calling, String... Params)
    {
        StringBuilder RequestStr = new StringBuilder(Host);
        switch (TypeQualification(rType))
        {
            case Project: RequestStr.append("/api/project");
                break;
            case Region: RequestStr.append("/api/region");
                break;
            case Route: RequestStr.append("/api/route");
                break;
            case RoutePoint: RequestStr.append("/api/route-point");
                break;
            case Section: RequestStr.append("/api/section");
                break;
            case SectionView: RequestStr.append("/api/section-view");
                break;
            case Town: RequestStr.append("/api/town");
                break;
            case RoutePointUi: RequestStr.append("/api/route-point-ui");
                break;
        }
        RequestStr.append("/index");
        boolean FirstParam = true;
        for (String p : Params)
        {
            if (FirstParam) RequestStr.append("?");
            else RequestStr.append("&");
            FirstParam = false;
            RequestStr.append(p);
        }
        //if (textView4 != null) textView4.setText(textView4.getText() + " \n" + RequestStr.toString());          //
        task = new ExchangeTask(this, RequestStr.toString(), rType, Calling);
        task.execute();
    }

    private EntityType TypeQualification(RequestType Type)
    {
        switch (Type)
        {
            case RegionList: return(EntityType.Region);
            case TownList: return(EntityType.Town);
            case TownListNear: return(EntityType.Town);
            case TownListPrename: return(EntityType.Town);
            case SectionList: return(EntityType.Section);
            case RouteList: return(EntityType.Route);
            case RouteListNear: return(EntityType.Route);
            case RoutPointList: return(EntityType.RoutePoint);
            case FinePointList: return (EntityType.RoutePointUi);
            default: return(null);
        }
    }


    private void SortListByText(ArrayList<Object> list)
    {
        int s = list.size();
        for (int i = 0; i < (s - 1); i++)
        {
            for (int j = (i + 1); j < s; j++)
            {
                if ((list.get(i) instanceof SortByText) && (list.get(j) instanceof SortByText))
                {
                    String iText = ((SortByText) list.get(i)).getSortText();
                    String jText = ((SortByText) list.get(j)).getSortText();
                    if (iText.compareTo(jText) > 0)
                    {
                        Object oTemp = list.get(i);
                        list.set(i, list.get(j));
                        list.set(j, oTemp);
                    }
                }
            }
        }
    }



    public class Sections implements SortByText
    {
        public int id;
        public String Name;
        //public String Description;
        //public String Comment;
        public String town_id;
        public int use_people_maps;
        public String getSortText() {
            return Name;
        }
    }

    public class Routes implements SortByText
    {
        public int id;
        public int section_id;
        public String name;
        public String description;
        public RouteLine.TransportType route_type;
        public String[] timetable;

        public String getSortText()
        {
            String SortText = name;
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(SortText);
            if (matcher.find(0)) {
                String subNumber = SortText.substring(matcher.start(), matcher.end());
                if (subNumber.length() == 1) SortText = SortText.replace(subNumber, "00" + subNumber);
                if (subNumber.length() == 2) SortText = SortText.replace(subNumber, "0" + subNumber);
            }
            switch (route_type) {
                case TROLLEYBUS:
                    return "3" + SortText;
                case BUS:
                    return "4" + SortText;
                case TRAM:
                    return "2" + SortText;
                case TRAIN:
                    return "5" + SortText;
                case METRO:
                    return "1" + SortText;
            }
            return "9" + SortText;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Routes)) return false;
            Routes routes = (Routes) o;
            if (id != routes.id) return false;
            if (section_id != routes.section_id) return false;
            if (!name.equals(routes.name)) return false;
            if (description != null ? !description.equals(routes.description) : routes.description != null)
                return false;
            return route_type == routes.route_type;
        }
    }

    public class RoutePoints //implements SortByText
    {
        public int id;
        public String Name;
        public double Latitude;
        public double Longitude;
        //public int Number;
        //public String getSortText() {
        //    Integer SortNumber = new Integer(Number);
        //    return SortNumber.toString();
        //}
    }


    public class Towns implements SortByText
    {
        public String id;
        public String region_id;
        public String Name;
        //public double Latitude;
        // public double Longitude;
        public String getSortText() {
            return Name;
        }
    }

    public class Regions implements SortByText
    {
        public String id;
        public String Name;
        public String getSortText() {
            return Name;
        }

    }


    private enum EntityType
    {
        Project,
        Region,
        Route,
        RoutePoint,
        Section,
        SectionView,
        Town,
        RoutePointUi
    }

    public enum RequestType
    {
        RegionList,
        TownList,
        TownListNear,
        TownListPrename,
        SectionList,
        RouteList,
        RouteListNear,
        RoutPointList,
        FinePointList
    }

    public interface SortByText
    {
        public String getSortText();
    }

}

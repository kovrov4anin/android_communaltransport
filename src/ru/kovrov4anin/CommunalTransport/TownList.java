package ru.kovrov4anin.CommunalTransport;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.widget.*;

/**
 * Created by User on 22.05.2015.
 */
public class TownList extends LinearLayout
{
    public TableRow.LayoutParams LayoutParams;
    private LineClicked.ListType listType;
    private TextView nTextView1;
    private Context context;
    private LineClicked Instance;

    public TownList(Context context, LineClicked.ListType Type)
    {
        super(context);
        this.context = context;
        this.listType = Type;
        LayoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        LayoutParams.setMargins(0, 5, 0, 5);
        LayoutParams.column = 0;
        LayoutParams.weight = 1;
        this.setOrientation(LinearLayout.VERTICAL);

        nTextView1 = new TextView(context);
        nTextView1.setTextAppearance(context, android.R.style.TextAppearance_Medium);
        nTextView1.setTypeface(Typeface.DEFAULT_BOLD);
        nTextView1.setGravity(Gravity.CENTER_HORIZONTAL);
        nTextView1.setClickable(false);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.gravity = Gravity.CENTER_HORIZONTAL;
        llp.setMargins(5, 5, 5, 5);
        this.addView(nTextView1, llp);
    }

    public void SetTitle(String Title)
    {
        //String htmlTaggedString  = "<u>" + Title + "</u>";
        //Spanned textSpan  =  android.text.Html.fromHtml(htmlTaggedString);
        nTextView1.setText(Title);
    }

    public void setTownList(String[] captions, LineClicked instance)
    {
        this.Instance = instance;
        for (int i = 0; i < captions.length; i++)
        {
            if (i > 0)
            {
                View nView = new View(context);
                nView.setBackgroundColor(0x7f7f7f7f);
                LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1);
                llp.setMargins(5, 0, 5, 0);
                this.addView(nView, llp);
            }
            TextView nTextView = new TextView(context);
            nTextView.setTextAppearance(context, android.R.style.TextAppearance_Medium);
            nTextView.setGravity(Gravity.CENTER_VERTICAL);
            nTextView.setId(i);
            nTextView.setText(captions[i]);
            nTextView.setClickable(true);
            nTextView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Instance != null)
                    {
                        Instance.ListLineClicked(listType, ((TextView)v).getText().toString(), v.getId());
                    }
                }
            });
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            llp.gravity = Gravity.LEFT;
            llp.setMargins(10, 15, 10, 15);
            this.addView(nTextView, llp);
        }

    }
}

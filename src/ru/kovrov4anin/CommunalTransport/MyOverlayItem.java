package ru.kovrov4anin.CommunalTransport;

import java.util.ArrayList;
import java.util.List;

import android.graphics.drawable.Drawable;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;


public class MyOverlayItem extends OverlayItem
{
    List<GeoPoint> geoPoint;
    ArrayList<ScreenPoint> screenPoint = new ArrayList<ScreenPoint>();

    public MyOverlayItem(List<GeoPoint> geoPoint) {
        super(geoPoint.get(0), null);
        this.geoPoint = geoPoint;
    }

    @Override
    public int compareTo(Object another) {
        return 0;
    }
}

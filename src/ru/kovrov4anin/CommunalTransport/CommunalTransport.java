package ru.kovrov4anin.CommunalTransport;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.*;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.google.gson.Gson;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import java.util.ArrayList;
import java.util.Date;


public class CommunalTransport extends Activity implements ReceivWebData
{
    private SharedPreferences mSettings;
    private Button mButton1;
    private Spinner mSpinner1;
    private ArrayAdapter<String> SpinnerAdapter;
    private TableRow mTableRow1;
    private TableRow mTableRow2;
    private RouteList NearRouts;
    private RouteList AllRouts;
    private ArrayList<Web> ConnectionList;
    private Web ConnectionNear;
    private ProgressDialog pd;
    private TownDataFull DataFullWeb;
    private TownDataFull DataFullArh;
    private TownDataFull DataVisual;
    private TownDataFull.SectionDataFull VisualSection;
    private Intent TownsIntent;
    private Geolocation geolocation;
    private GeoPoint SavedLocation;


    private void VisualData(TownDataFull Data)
    {
        DataVisual = Data;
        mButton1.setText(DataVisual.TownName);
        RemoveRoutLists();
        String[] SpinnerList = new String[DataVisual.SectionsData.length];
        for (int i = 0; i < DataVisual.SectionsData.length; i++) {
            SpinnerList[i] = DataVisual.SectionsData[i].Section.Name;
        }
        SpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, SpinnerList);
        mSpinner1.setAdapter(SpinnerAdapter);
        boolean isSpinnerSelectionFind = false;
        int LastSectionId = mSettings.getInt("LastSection", -1);
        for (int i = 0; i < Data.SectionsData.length; i++) {
            if (Data.SectionsData[i].Section.id == LastSectionId) {
                mSpinner1.setSelection(i);
                isSpinnerSelectionFind = true;
            }
        }
        if((!isSpinnerSelectionFind) && (DataVisual.SectionsData.length > 0)) mSpinner1.setSelection(0);
    }


    private void VisualSectionData(TownDataFull.SectionDataFull sdf)
    {
        mTableRow2.removeAllViews();
        if (sdf.RoutesData.length > 0)
        {
            AllRouts = new RouteList(this);
            AllRouts.SetTitle(getString(R.string.routs_text));
            for (int i = 0; i < sdf.RoutesData.length; i++) {
                AllRouts.AddRoute(sdf.RoutesData[i]);
            }
            AllRouts.AddInfoLine();
            mTableRow2.addView(AllRouts, AllRouts.LayoutParams);
        }
    }


    private void VisualNearData(TownDataFull.SectionDataFull sdf)
    {
        mTableRow1.removeAllViews();
        if ((sdf.RoutesNear != null) && (sdf.RoutesNear.size() > 0))
        {
            NearRouts = new RouteList(this);
            NearRouts.SetTitle(getString(R.string.routs_near_text));
            for (int i = 0; i < sdf.RoutesNear.size(); i++) {
                NearRouts.AddRoute(sdf.RoutesNear.get(i));
            }
            mTableRow1.addView(NearRouts, NearRouts.LayoutParams);
        }
    }


    public void receiveList(Web.RequestType Type, ArrayList<?> list, Object Calling)
    {
        if ((list == null) || (Calling == null)) return;
        if (Type == Web.RequestType.SectionList)
        {
            TownDataFull tdf = (TownDataFull)Calling;
            tdf.initSectionsData(list.size());
            for (int i = 0; i < list.size(); i++) {
                tdf.SectionsData[i].Section = (Web.Sections)list.get(i);
                String param1 = "section_id=" + tdf.SectionsData[i].Section.id;
                if (ConnectionList != null) {
                    Web webConection = new Web(this);
                    webConection.RequestData(getString(R.string.url_name), Web.RequestType.RouteList, DataFullWeb.SectionsData[i], param1);
                    ConnectionList.add(webConection);
                }
            }
        }
        if (Type == Web.RequestType.RouteList)
        {
            TownDataFull.SectionDataFull sdf = (TownDataFull.SectionDataFull)Calling;
            sdf.initRoutesData(list.size());
            for (int i = 0; i < list.size(); i++) {
                sdf.RoutesData[i].Route = (Web.Routes)list.get(i);
                sdf.RoutesData[i].UsePeopleMap = sdf.Section.use_people_maps;
                String param1 = "route_id=" + sdf.RoutesData[i].Route.id;
                if (ConnectionList != null) {
                    Web webConection = new Web(this);
                    webConection.RequestData(getString(R.string.url_name), Web.RequestType.RoutPointList, sdf.RoutesData[i], param1);
                    ConnectionList.add(webConection);
                }
            }
        }
        if (Type == Web.RequestType.RouteListNear)
        {
            TownDataFull.SectionDataFull sdf = (TownDataFull.SectionDataFull)Calling;
            sdf.RoutesNear = null;
            for (int i = 0; i < list.size(); i++) {
                sdf.AddFindRoute((Web.Routes) list.get(i));
            }
            VisualNearData(sdf);
        }
        if (Type == Web.RequestType.RoutPointList)
        {
            TownDataFull.SectionDataFull.RouteDataFull rdf = (TownDataFull.SectionDataFull.RouteDataFull)Calling;
            rdf.initRoutePointsData(list.size());
            for (int i = 0; i < list.size(); i++) {
                rdf.RoutePointsData[i] = (Web.RoutePoints)list.get(i);
            }
        }
        if (DataFullWeb != null)
        {
            if (DataFullWeb.isFilled())
            {
                if ((pd != null) && (pd.isShowing()))
                {
                    pd.hide();
                    pd = null;
                    VisualData(DataFullWeb);
                }
                ConnectionList.clear();
                Gson gson = new Gson();
                String json = gson.toJson(DataFullWeb);
                SharedPreferences.Editor editor = mSettings.edit();
                editor.putString("DataFull", json);
                editor.putLong("lastDateLong", (new Date()).getTime());
                editor.apply();
            }
        }
    }


    private void RemoveRoutLists()
    {
        mTableRow1.removeView(NearRouts);
        NearRouts = null;
        mTableRow2.removeView(AllRouts);
        AllRouts = null;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        TownsIntent = new Intent(this, TownsChoice.class);
        mTableRow1 = (TableRow)findViewById(R.id.TableRow1);
        mTableRow2 = (TableRow)findViewById(R.id.TableRow2);
        mButton1 = (Button)findViewById(R.id.button1);
        mSpinner1 = (Spinner)findViewById(R.id.spinner1);
        mSpinner1.setOnItemSelectedListener(onItemSelectedListener);
        SavedLocation = null;
        geolocation = new Geolocation(this, 100) {
            @Override
            void ReceiveLocation(double Latitude, double Longitude) {
                SavedLocation = new GeoPoint(Latitude, Longitude);
                if ((isOnline()) && (VisualSection != null)) NearRoutesRequest(SavedLocation);
            }
        };
        mSettings = getSharedPreferences("CrossTownPreferences", Context.MODE_PRIVATE);
        String json = mSettings.getString("DataFull", "");
        if (!json.isEmpty()) DataFullArh = new Gson().fromJson(json, TownDataFull.class);
        if (isOnline())
        {
            if ((DataFullArh != null) && (!DataFullArh.TownName.isEmpty()) && (!DataFullArh.TownId.isEmpty()))
            {
                Date cDate = new Date();
                long lastDateLong = mSettings.getLong("lastDateLong", 0);
                Date lDate = new Date(lastDateLong);
                if ((DataFullArh.isFilled()) && (cDate.getYear() == lDate.getYear())
                        && (cDate.getMonth() == lDate.getMonth()) && (cDate.getDate() == lDate.getDate())) {
                    VisualData(DataFullArh);
                }
                else {
                    RequestWebData(DataFullArh.TownName, DataFullArh.TownId);
                }

            }
            else
            {
                startActivityForResult(TownsIntent, 1);
            }
        }
        else
        {
            if ((DataFullArh != null) && (DataFullArh.isFilled()))
            {
                VisualData(DataFullArh);
                mButton1.setEnabled(false);
            }
            else
            {
                mButton1.setEnabled(false);
                AlertDialog.Builder ad = new AlertDialog.Builder(this);
                ad.setTitle(R.string.Error_title_text);
                ad.setMessage(R.string.Error_message_text);
                ad.setIcon(android.R.drawable.ic_dialog_alert);
                ad.setNegativeButton(R.string.cancel_text, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CommunalTransport.this.finish();
                    }
                });
                ad.show();
            }
        }



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        geolocation.RemoveUpdates();
    }



    private void NearRoutesRequest(GeoPoint gp)
    {
        String param1 = "section_id=" + VisualSection.Section.id;
        String param2 = "latitude=" + gp.getLat();
        String param3 = "longitude=" + gp.getLon();
        ConnectionNear = new Web(this);
        ConnectionNear.RequestData(getString(R.string.url_name), Web.RequestType.RouteListNear, VisualSection, param1, param2, param3);
    }


    public void onButton1Click(View view)
    {
        startActivityForResult(TownsIntent, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (data != null)
        {
            if ((requestCode == 1) && (resultCode == RESULT_OK))
            {
                RequestWebData(data.getStringExtra("TownName"), data.getStringExtra("TownId"));
            }
        }
    }


    private void RequestWebData(String TownName, String TownId)
    {
        DataFullWeb = new TownDataFull();
        DataFullWeb.TownName = TownName;
        DataFullWeb.TownId = TownId;
        String param1 = "town_id=" + DataFullWeb.TownId;
        ConnectionList = new ArrayList<Web>();
        ConnectionList.add(new Web(this));
        ConnectionList.get(0).RequestData(getString(R.string.url_name), Web.RequestType.SectionList, DataFullWeb, param1);
        pd = new ProgressDialog(this);
        pd.setTitle(getString(R.string.progress_title_text));
        pd.setMessage(getString(R.string.progress_routs_message_text));
        pd.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.cancel_text), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                if ((DataFullArh != null) && (DataFullArh.isFilled())) VisualData(DataFullArh);
                else CommunalTransport.this.finish();
            }
        });
        pd.show();
    }



    AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
        {
            RemoveRoutLists();
            int SectionIndex = mSpinner1.getSelectedItemPosition();
            if ((DataVisual != null) && (DataVisual.isFilled()))
            {
                VisualSection = DataVisual.SectionsData[SectionIndex];
                VisualSectionData(VisualSection);
                if (SavedLocation != null) NearRoutesRequest(SavedLocation);
                SharedPreferences.Editor editor = mSettings.edit();
                editor.putInt("LastSection", VisualSection.Section.id);
                editor.apply();
            }
        }
        public void onNothingSelected(AdapterView<?> parent) {  }
    };



    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnectedOrConnecting());
    }


}

package ru.kovrov4anin.CommunalTransport;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class RouteList extends LinearLayout
{
    private Context context;
    public TableRow.LayoutParams LayoutParams;
    private TextView nTextView1;
    private int RowCount;
    private Dialog dialog;
    private TextView TextView6;


    public RouteList(Context context)
    {
        super(context);
        this.context = context;
        LayoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        LayoutParams.setMargins(0, 5, 0, 5);
        LayoutParams.column = 0;
        LayoutParams.weight = 1;
        this.setOrientation(LinearLayout.VERTICAL);

        nTextView1 = new TextView(context);
        nTextView1.setTextAppearance(context, android.R.style.TextAppearance_Medium);
        nTextView1.setTypeface(Typeface.DEFAULT_BOLD);
        nTextView1.setGravity(Gravity.CENTER_HORIZONTAL);
        nTextView1.setClickable(false);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.gravity = Gravity.CENTER_HORIZONTAL;
        llp.setMargins(5, 5, 5, 5);
        this.addView(nTextView1, llp);

        RowCount = 0;
    }


    public void SetTitle(String Title)
    {
        nTextView1.setText(Title);
    }


    public void AddRoute(TownDataFull.SectionDataFull.RouteDataFull rdf)
    {
        AddSeparator();
        RouteLine rl = new RouteLine(context, rdf);
        this.addView(rl, rl.LayoutParams);
        RowCount++;
    }


    public void AddInfoLine()
    {
        AddSeparator();
        TextView nTextView10 = new TextView(context);
        nTextView10.setTextAppearance(context, android.R.style.TextAppearance_Medium);
        nTextView10.setText(context.getString(R.string.about_programm_text));
        nTextView10.setGravity(Gravity.CENTER);
        nTextView10.setClickable(true);
        nTextView10.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowAboutDialog();
            }
        });
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.gravity = Gravity.CENTER_VERTICAL;
        llp.setMargins(15, 15, 15, 15);
        this.addView(nTextView10, llp);
        RowCount++;
    }


    private void AddSeparator()
    {
        if (RowCount > 0)
        {
            View nView = new View(context);
            nView.setBackgroundColor(0x7f7f7f7f);
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1);
            llp.setMargins(5, 0, 5, 0);
            this.addView(nView, llp);
        }
    }


    private void ShowAboutDialog()
    {
        dialog = new Dialog(context);
        dialog.setTitle(context.getString(R.string.about_programm_text));
        dialog.setContentView(R.layout.about_info);

        TextView6 = (TextView)dialog.findViewById(R.id.textView6);
        TextView6.setLinksClickable(true);
        TextView6.setMovementMethod(new LinkMovementMethod());

        dialog.show();
    }

}
